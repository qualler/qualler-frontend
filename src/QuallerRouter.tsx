import * as React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import App from './App';
import TrainingsPage from './trainings/TrainingsPage';
import QuallerDrawer from './drawer/QuallerDrawer';
import TrainingDetailsPage from './trainings/training-details/TrainingDetailsPage';
import CoachesPage from './coaches/CoachesPage';
import CoachDetailsPage from './coaches/coach-details/CoachDetailsPage';
import LocationsPage from './locations/LocationsPage';
import LocationDetailsPage from './locations/location-details/LocationDetailsPage';

interface Props {
}

class QuallerRouter extends React.PureComponent<Props, any> {
    render() {
        return (
            <div className="app-root">
                <BrowserRouter>
                    <div className={'app__content'}>
                        <QuallerDrawer />
                        <Switch>
                            <Route exact={true} path={'/'} component={App} />
                            <Route exact={true} path={'/trainings'} component={TrainingsPage} />
                            <Route exact={true} path={'/trainings/:trainingDateId'} component={TrainingDetailsPage} />
                            <Route exact={true} path={'/coaches'} component={CoachesPage} />
                            <Route exact={true} path={'/coaches/:coachId'} component={CoachDetailsPage} />
                            <Route exact={true} path={'/locations'} component={LocationsPage} />
                            <Route exact={true} path={'/locations/:locationId'} component={LocationDetailsPage} />
                        </Switch>
                    </div>
                </BrowserRouter>
            </div>
        );
    }
}

export default QuallerRouter;
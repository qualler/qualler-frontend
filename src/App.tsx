import * as React from 'react';
import CreateTrainingDatePage from './training-date/CreateTrainingDatePage';

require('react-md/dist/react-md.blue-pink.min.css');
require('./App.css');

const App = () => (
    <CreateTrainingDatePage />
);

export default App;
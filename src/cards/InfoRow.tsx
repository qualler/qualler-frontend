import * as React from 'react';
import { Divider } from 'react-md';
require('./InfoRow.css');

interface Props {
    text: string;
}

class InfoRow extends React.PureComponent<Props, any> {
    render() {
        const { text, children } = this.props;
        return (
            <div className="column info-row">
                <div className="row align--center info-row__content">
                    <div className="info-row__title">
                        <span>{text}</span>
                    </div>
                    <div className="info-row__value">
                        {children}
                    </div>
                </div>
                <Divider />
            </div>
        );
    }
}

export default InfoRow;
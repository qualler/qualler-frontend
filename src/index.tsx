import * as React from 'react';
import * as ReactDOM from 'react-dom';

import QuallerRouter from './QuallerRouter';

ReactDOM.render(
    <QuallerRouter />,
    document.getElementById('root') as HTMLElement
);
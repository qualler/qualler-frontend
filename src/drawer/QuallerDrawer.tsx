import * as React from 'react';
import { Paper, Button, List, ListItem, FontIcon } from 'react-md';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';

interface Props {
}

require('./QuallerDrawer.css');

const links: DrawerLink[] = [
    {
        title: 'Terminy szkoleń',
        path: '/trainings',
        icon: 'calendar_today'
    },
    {
        title: 'Lokalizacje',
        path: '/locations',
        icon: 'account_balance'
    },
    {
        title: 'Trenerzy',
        path: '/coaches',
        icon: 'face'
    },

];
class QuallerDrawer extends React.PureComponent<Props & RouteComponentProps<any>, any> {

    render() {
        const { history } = this.props;
        return (
            <Paper className={'qualler-drawer qualler-drawer--dark'}>
                <AccountSlot />
                <div>
                    <Links
                        onLinkClicked={link => history.push(link.path)}
                        links={links} />
                </div>
            </Paper>
        );
    }
}

const AccountSlot = () => (
    <div className={'drawer__account-slot'}>
        <AvatarSlot />
        <BottomPanel />
    </div>
);

const AvatarSlot = () => (
    <div className={'account-slot__avatar-container'}>
        <div className={'account-slot__avatar'}></div>
    </div>);

const BottomPanel = () => (
    <div className={'account-slot__bottom-panel'}>
        <span className={'bottom-panel__username'}>@danny.sullivan</span>
        <Button
            className={'bottom-panel__logout'}
            inkClassName={'bottom-panel__logout__ink'}
            flat={true}>
            <span style={{ color: '#f0f0f0' }}>Wyloguj</span>
        </Button>
    </div>);

interface DrawerLink {
    title: string;
    path: string;
    icon: string;
}
const Links = (props: { links: DrawerLink[]; onLinkClicked(link: DrawerLink): void }) => (
    <div className="drawer__links column">
        {props.links.map((link, i) => (
            <div
                onClick={() => props.onLinkClicked(link)}
                className="drawer__link row align--center" key={`drawer-link-${i}`}>
                <div className="drawer__link__icon-container">
                    <FontIcon className="drawer__link__icon">{link.icon}</FontIcon>
                </div>
                <div className="drawer__link__text-container">
                    <span className="drawer__link__text">{link.title}</span>
                </div>
            </div>
        ))}
    </div>
);

export default withRouter(QuallerDrawer);
import * as React from 'react';
require('./SmallError.css');

interface Props {
    error?: string;
}

class SmallError extends React.PureComponent<Props, any> {
    render() {
        const { error } = this.props;
        if (!error) {
            return null;
        }
        return (
            <div className="small-error__container">
                {error && <span className="small-error__message">{error}</span>}
            </div>
        );
    }
}

export default SmallError;
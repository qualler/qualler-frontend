import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Card, Toolbar } from 'react-md';
import LocationsList from '../training-date/organization-way/location/LocationsList';

interface Props {
}

class LocationsPage extends React.PureComponent<Props & RouteComponentProps<any>, any> {

    render() {
        const { history } = this.props;
        return (
            <div className="page__content">
                <Card className="column" style={{ width: '90%', marginTop: 60 }}>
                    <Toolbar colored={true} title="Lokalizacje szkoleń" />
                    <LocationsList
                        onLocationChosen={location => history.push(`/locations/${location.id}`)}
                    />
                </Card>
            </div>
        );
    }
}

export default LocationsPage;
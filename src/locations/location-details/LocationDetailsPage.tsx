import * as React from 'react';
import { Location, DetailedTrainingDate, formatMoney } from '../../training-date/qualler-types';
import { RouteComponentProps } from 'react-router';
import { LocationApi } from '../../training-date/organization-way/location/LocationApi';
import { Card, Toolbar } from 'react-md';
import InfoRow from '../../cards/InfoRow';
import TrainingsList from '../../trainings/TrainingsList';

interface Props {
}

interface State {
    location: Location | null;
    trainings: DetailedTrainingDate[];
}

interface UrlProps {
    locationId: string;
}

class LocationDetailsPage extends React.PureComponent<Props & RouteComponentProps<UrlProps>, State> {

    constructor(props: any) {
        super(props);
        this.state = {
            location: null,
            trainings: []
        };
    }

    render() {
        const { location, trainings } = this.state;
        if (!location) {
            return null;
        }
        return (
            <div className="page__content">
                <div style={{ marginTop: 60, marginBottom: 10, width: '90%' }} className="row">
                    <span className="md-display-1">Szczegóły lokalizacji</span>
                </div>

                <Card className="row content--space-between" style={{ width: '90%' }}>
                    <div className="column align--center"
                        style={{ padding: '20px', borderRight: '1px solid #cccccc', width: '30%' }}>
                        <div className="column">
                            {location.address.first_line &&
                                <span
                                    style={{ marginBottom: 0 }}
                                    className="md-headline">{location.address.first_line}</span>}
                            <span
                                style={{ marginBottom: 0 }}
                                className="md-subheading-2">{location.address.second_line}</span>
                            <span className="md-body-2">{location.address.city}, {location.address.zip_code}</span>
                        </div>
                    </div>
                    <div className="column" style={{ width: '100%' }}>
                        <InfoRow text={'Dzienny koszt wynajęcia:'}>
                            <span>{formatMoney(location.daily_rental_costs)}</span>
                        </InfoRow>
                    </div>
                </Card>
                <br/>
                <Card style={{ width: '90%' }}>
                    <Toolbar colored={true} title="Szkolenia organizowane w tej lokalizacji" />
                    {trainings.length > 0 ? 
                    <TrainingsList 
                        trainings={trainings}
                    />
                    :
                    <div style={{padding: 20}}>
                        <span className="md-body-1">W tej lokalizacji obecnie nie 
                        są zorganizowane żadne szkolenia</span>
                    </div>}
                </Card>
            </div>
        );
    }

    async componentDidMount() {
        const locationId = parseInt(this.props.match.params.locationId, 10);
        const location = await LocationApi.fetchLocation(locationId);
        this.setState({ location });

        const trainings = await LocationApi.fetchOrganizedTrainings(locationId);
        this.setState({ trainings });
    }
}

export default LocationDetailsPage;
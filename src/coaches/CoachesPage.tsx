import * as React from 'react';
import { User } from '../training-date/qualler-types';
import { CoachApi } from '../training-date/coach/CoachApi';
import CoachesList from '../training-date/coach/CoachesList';
import { Card, Toolbar } from 'react-md';
import { RouteComponentProps } from 'react-router';

interface Props {
}

class CoachesPage extends React.PureComponent<Props & RouteComponentProps<any>, any> {

    render() {
        const { history } = this.props;
        return (
            <div className="page__content">
                <Card className="column" style={{ width: '90%', marginTop: 60 }}>
                    <Toolbar colored={true} title="Trenerzy" />
                    <CoachesList
                        onCoachChosen={coach => history.push(`/coaches/${coach.id}`)}
                    />
                </Card>
            </div>
        );
    }
}

export default CoachesPage;
import * as React from 'react';
import { Card, Toolbar, Avatar, Chip } from 'react-md';
import { User, DetailedTrainingDate, formatMoney } from '../../training-date/qualler-types';
import { CoachApi } from '../../training-date/coach/CoachApi';
import { RouteComponentProps } from 'react-router';
import TrainingsList from '../../trainings/TrainingsList';
import InfoRow from '../../cards/InfoRow';
require('./CoachDetailsPage.css');

interface Props {

}

interface State {
    coach: User | null;
    trainings: DetailedTrainingDate[];
}

interface UrlProps {
    coachId: string;
}

class CoachDetailsPage extends React.PureComponent<Props & RouteComponentProps<UrlProps>, State> {

    constructor(props: any) {
        super(props);
        this.state = {
            coach: null,
            trainings: []
        };
    }

    render() {
        const { coach, trainings } = this.state;
        if (!coach) {
            return null;
        }
        return (
            <div className="page__content">
                <div style={{ marginTop: 60, marginBottom: 10, width: '90%' }} className="row">
                    <span className="md-display-1">Szczegóły trenera</span>
                </div>

                <Card className="row content--space-between" style={{ width: '90%' }}>
                    <div className="column align--center"
                        style={{ padding: '20px', borderRight: '1px solid #cccccc', width: '30%' }}>
                        <Avatar style={{ width: 80, height: 80, marginBottom: 20 }} suffix="blue">
                            <span style={{ fontSize: 30 }}>
                                {coach.first_name.substring(0, 1) + coach.last_name.substring(0, 1)}
                            </span>
                        </Avatar>
                        <span className="md-headline">
                            {coach.first_name} {coach.last_name}
                        </span>
                    </div>
                    <div className="column" style={{ width: '70%' }}>
                        <InfoRow text={'Dzienne wynagrodzenie:'}>
                            <span>{coach.profiles.COACH &&
                                formatMoney((coach.profiles.COACH.daily_salary))}
                            </span>
                        </InfoRow>
                        <InfoRow text={'Certyfikaty:'}>
                            {coach.profiles.COACH && coach.profiles.COACH
                                .certificates
                                .map((certificate, i) => (
                                    <Chip key={`certificate-${i}`}
                                        style={{ marginRight: 10 }}
                                        label={certificate} />
                                ))}
                        </InfoRow>
                    </div>
                </Card>
                <br />
                <Card style={{ width: '90%' }}>
                    <Toolbar colored={true} title="Prowadzone szkolenia" />
                    {trainings.length > 0 ?
                        <TrainingsList trainings={trainings} />
                        :
                        <div style={{ padding: 20 }}>
                            <span className="md-body-1">Trener aktualnie nie prowadzi żadnych szkoleń</span>
                        </div>
                    }
                </Card>
            </div>
        );
    }

    async componentDidMount() {
        const coachId = parseInt(this.props.match.params.coachId, 10);
        const coach = await CoachApi.fetchCoach(coachId);
        this.setState({ coach });

        const conductedTrainings = await CoachApi.fetchConductedTrainings(coach.id);
        this.setState({ trainings: conductedTrainings });
    }
}

export default CoachDetailsPage;
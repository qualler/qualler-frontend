import * as React from 'react';
import { Training, OrganizationWay, CertificationPolicy, User, TrainingDate } from './qualler-types';
import BasicInfo from './BasicInfo';
import TrainingOrganizationWay from './organization-way/TrainingOrganizationWay';
import CertificationCard from './certification/CertificationCard';
import CoachCard from './coach/CoachCard';
import { Button, TextField, Divider } from 'react-md';
import { TrainingDateApi } from './TrainingDateApi';
require('./TrainingDateForm.css');

interface Props extends React.HTMLAttributes<HTMLDivElement> {
}

interface State {
    availableTrainings: Training[];
    selectedTraining: Training | null;
    closed: boolean;
    startDate: Date | null;
    endDate: Date | null;
    organizationWays: OrganizationWay[];
    certificationPolicy: CertificationPolicy;
    coach: User | null;
    decimals: string;
    wholes: string;
    errors: FormErrors;
}

interface FormErrors {
    selectedTraining?: string;
    startDate?: string;
    endDate?: string;
    organizationWays?: string;
    coach?: string;
    certificationPolicy?: string;
    wholes?: string;
    decimals?: string;
}

class TrainingDateForm extends React.PureComponent<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            availableTrainings: [],
            selectedTraining: null,
            startDate: null,
            endDate: null,
            closed: false,
            decimals: '',
            wholes: '',
            organizationWays: [],
            certificationPolicy: {
                type: 'no-certification'
            },
            coach: null,
            errors: {}
        };
    }

    render() {
        const { className } = this.props;
        const { selectedTraining,
            closed,
            organizationWays,
            certificationPolicy,
            coach,
            startDate,
            errors,
            wholes,
            decimals,
            endDate } = this.state;
        return (
            <div className={`${className} qualler-training-form`}>
                <BasicInfo
                    trainingError={errors.selectedTraining}
                    startDateError={errors.startDate}
                    endDateError={errors.endDate}
                    training={selectedTraining}
                    startDate={startDate}
                    onStartDateChange={value => this.setState({ startDate: value })}
                    endDate={endDate}
                    onEndDateChange={value => this.setState({ endDate: value })}
                    onTrainingSelected={training =>
                        this.setState({ selectedTraining: training })} >
                        <Divider />
                    <div style={{ padding: 20 }}>
                        <span className="md-title">Cena szkolenia</span>
                        <div className="row">
                            <TextField
                                error={!!(errors.wholes)}
                                errorText={errors.wholes}
                                value={wholes}
                                onChange={value => this.setState({ wholes: value.toString() })}
                                style={{ width: 150 }}
                                id="rental-wholes"
                            />
                            <TextField
                                value={decimals}
                                error={!!(errors.decimals)}
                                errorText={errors.decimals}
                                onChange={value => this.setState({ decimals: value.toString() })}
                                style={{ width: 200 }}
                                leftIcon={<span>,</span>}
                                rightIcon={<span>PLN</span>}
                                id="rental-decimals"
                            />
                        </div>
                    </div>
                </BasicInfo>
                <br />
                <TrainingOrganizationWay
                    error={errors.organizationWays}
                    organizationWays={organizationWays}
                    onOrganizationChanged={value => this.setState({ organizationWays: value })}
                    trainingClosed={closed}
                    onClosedChange={value => this.setState({ closed: value })}
                />
                <br />
                <CertificationCard
                    error={errors.certificationPolicy}
                    certificationPolicy={certificationPolicy}
                    onCertificationPolicyChange={policy => this.setState({ certificationPolicy: policy })}
                />
                <br />
                <CoachCard
                    error={errors.coach}
                    coach={coach}
                    onCoachChosen={value => this.setState({ coach: value })}
                />
                <br />
                <div className="row content--end">
                    <Button onClick={this.saveForm} raised={true} primary={true}>ZAPISZ</Button>
                </div>
            </div>
        );
    }

    private saveForm = async () => {
        const errors = this.validate();
        this.setState({ errors });
        const { selectedTraining,
            closed,
            organizationWays,
            wholes,
            decimals,
            certificationPolicy,
            coach,
            startDate,
            endDate } = this.state;

        if (Object.keys(errors).length > 0) {
            return;
        }

        const trainingDate: TrainingDate = {
            id: -1,
            training_id: (selectedTraining as Training).id,
            coach_id: (coach as User).id,
            start_date: (startDate as Date).toISOString(),
            end_date: (endDate as Date).toISOString(),
            certification_policy: certificationPolicy,
            open: !closed,
            organization_ways: organizationWays,
            price: {
                wholes: parseInt(wholes, 10),
                decimals: parseInt(decimals, 10),
                currency: 'PLN'
            }
        };

        const response = await TrainingDateApi.createTrainingDate(trainingDate);
        if (response.ok) {
            alert('Termin szkolenia został pomyślnie dodany');
        }
    }
    private validate(): FormErrors {
        const errors: FormErrors = {};
        const { selectedTraining,
            closed,
            organizationWays,
            wholes,
            decimals,
            certificationPolicy,
            coach,
            startDate,
            endDate } = this.state;

        if (!selectedTraining) {
            errors.selectedTraining = 'Musisz wybrać szkolenie';
        }
        if (!wholes) {
            errors.wholes = 'Wartość jest wymagana';
        } else if (!(/^[1-9][0-9]{0,6}$/).test(wholes)) {
            errors.wholes = 'Nieprawidłowa wartość';
        }

        if (!decimals) {
            errors.decimals = 'Wartość jest wymagana';
        } else if (!(/^[0-9]{2}$/).test(decimals)) {
            errors.decimals = 'Nieprawidłowa wartość';
        }
        if (!startDate) {
            errors.startDate = 'Wartość jest wymagana';
        }
        if (!endDate) {
            errors.endDate = 'Wartość jest wymagana';
        }
        if (organizationWays.length === 0 && !closed) {
            errors.organizationWays = 'Musisz podać przynajmniej jeden sposób organizacji';
        }
        if (!coach) {
            errors.coach = 'Musisz wybrać trenera';
        }
        if (certificationPolicy.type === 'certification-required' && certificationPolicy.certificates.length === 0) {
            errors.certificationPolicy = 'Szkolenie certyfikowane wymaga podania certyfikatów';
        }
        if (coach && certificationPolicy.type === 'certification-required') {
            if (coach.profiles.COACH && certificationPolicy.certificates.length > 0) {
                const coachCertes = coach.profiles.COACH.certificates;
                if (certificationPolicy.certificates
                    .some((certificate: string) => coachCertes.indexOf(certificate) === -1)) {
                    errors.coach = 'Trener nie posiada wymaganych certyfikatów';
                }
            }
        }
        return errors;
    }
}

export default TrainingDateForm;
import * as React from 'react';
import TrainingDateForm from './TrainingDateForm';

interface Props {
}

require('./CreateTrainingDatePage.css');

class CreateTrainingDatePage extends React.PureComponent<Props, any> {

    render() {
        return (
            <div className="create-training-page">
                <PageHeader />
                <TrainingDateForm className="create-training-page__segment" />
            </div>
        );
    }
}

const PageHeader = () => (
    <div className="create-training-page__segment segment--high">
        <span className="create-training-page__title">Nowy termin szkolenia</span>
    </div>
);

export default CreateTrainingDatePage;
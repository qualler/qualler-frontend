import * as React from 'react';
import { Paper, Toolbar, Autocomplete, DatePicker } from 'react-md';
import { Training } from './qualler-types';
import { TrainingsApi } from './TrainingsApi';
import * as moment from 'moment';

interface Props {
    startDate: Date | null;
    training: Training | null;
    endDate: Date | null;
    trainingError?: string;
    startDateError?: string;
    endDateError?: string;
    onStartDateChange(value: Date): void;
    onEndDateChange(value: Date): void;
    onTrainingSelected(training: Training): void;
}

interface State {
    trainings: Training[];
}

class BasicInfo extends React.PureComponent<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            trainings: []
        };
    }

    render() {
        const { training,
            onTrainingSelected,
            startDate,
            endDate,
            onStartDateChange,
            onEndDateChange,
            endDateError,
            startDateError,
            trainingError } = this.props;
        const { trainings } = this.state;
        return (
            <Paper>
                <Toolbar
                    colored
                    title="Informacje podstawowe"
                />
                <div className="qualler-training-form__basic-info">
                    <div className="basic-info__training-name-container">
                        <Autocomplete
                            defaultValue={training ? training.id : undefined}
                            id="chosen-training"
                            label="Wybierz szkolenie"
                            dataLabel={'name'}
                            dataValue={'id'}
                            error={!!trainingError}
                            helpText={trainingError}
                            data={trainings.map(_training => ({
                                ..._training,
                                datalabel: _training.id
                            }))}
                            onAutocomplete={(_trainingId: number, trainingIndex: number) =>
                                onTrainingSelected(trainings[trainingIndex])}
                            filter={Autocomplete.caseInsensitiveFilter}
                        />
                    </div>
                    <div className="basic-info__dates-container">
                        <DatePicker
                            icon={null}
                            minDate={new Date()}
                            error={!!startDateError}
                            errorText={startDateError}
                            value={startDate ? startDate : undefined}
                            onChange={(_formatted: string, date: Date) => {
                                onStartDateChange(date);
                                if (endDate && date > endDate) {
                                    onEndDateChange(moment(date).add(1, 'day').toDate());
                                }
                            }}
                            id="training-start-date"
                            label="Data rozpoczęcia"
                            className="md-cell"
                            displayMode="portrait"
                        />
                        <DatePicker
                            icon={null}
                            disabled={!startDate}
                            error={!!endDateError}
                            errorText={endDateError}
                            minDate={startDate ? startDate : undefined}
                            maxDate={startDate ? moment(startDate).add(10, 'days').toDate() : undefined}
                            value={endDate ? endDate : undefined}
                            onChange={(_formatted: string, date: Date) => onEndDateChange(date)}
                            id="training-end-date"
                            label="Data zakończenia"
                            className="md-cell"
                            displayMode="portrait"
                        />
                    </div>
                </div>
                {this.props.children}
            </Paper>
        );
    }

    async componentDidMount() {
        const trainings = await TrainingsApi.fetchTrainings();
        this.setState({
            trainings
        });
    }
}

export default BasicInfo;
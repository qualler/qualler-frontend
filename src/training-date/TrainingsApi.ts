import { Training } from './qualler-types';

const apiOrigin = 'http://localhost:8080';

export class TrainingsApi {

    static async fetchTrainings(): Promise<Training[]> {
        return fetch(`${apiOrigin}/api/trainings`)
            .then(response => response.json());
    }
}
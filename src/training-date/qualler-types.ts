
export interface Training {
    id: number;
    name: string;
}

export interface Location {
    id: number;
    address: Address;
    daily_rental_costs: Money;
}

export interface Address {
    first_line: string;
    second_line: string;
    zip_code: string;
    city: string;
}

export interface Money {
    wholes: number;
    decimals: number;
    currency: string;
}

export type CertificationPolicy = {
    type: 'no-certification'
} | {
    type: 'certification-required',
    certificates: string[]
};

export interface User {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
    profiles: {
        COACH?: {
            type: 'coach',
            daily_salary: Money
            certificates: string[]
        }
    };
}

export interface TrainingDate {
    id: number;
    training_id: number;
    organization_ways: OrganizationWay[];
    open: boolean;
    start_date: string;
    end_date: string;
    coach_id: number;
    certification_policy: CertificationPolicy;
    price: Money;
}

export interface DetailedTrainingDate extends TrainingDate {
    training: Training;
    coach: User;
}

export type OrganizationWay = { type: 'location', location_id: number } |
{ type: 'internet', video_stream_url: string };

export function formatMoney(money: Money): string {
    return `${money.wholes},${money.decimals < 10 ? '0' : ''}${money.decimals} ${money.currency}`;
}
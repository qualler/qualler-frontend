import * as React from 'react';
import { Avatar, FontIcon, Button } from 'react-md';
import { OrganizationWay, Location } from '../qualler-types';
import { LocationApi } from './location/LocationApi';
require('./OrganizationWaysList.css');

interface Props {
    organizationWays: OrganizationWay[];
    onOrganizationWaysChanged?(organizationWays: OrganizationWay[]): void;
    onLocationClick?(location: Location): void;
}

interface State {
    locations: FetchedLocations;
}

type FetchedLocations = { [key: number]: Location };

class OrganizationWaysList extends React.PureComponent<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            locations: {}
        };
    }

    render() {
        const { organizationWays } = this.props;
        return (
            <div>
                <div className="organization-way__list list--dark">
                    {organizationWays.map(this.asListItem)}
                </div>
            </div>
        );
    }

    componentDidUpdate() {
        const { organizationWays } = this.props;
        const { locations } = this.state;
        const locationIds = organizationWays
            .map(_organizationWay => _organizationWay.type === 'location' ? _organizationWay.location_id : -1)
            .filter(_id => _id !== -1);

        let needsRefresh = false;
        for (const locationId of locationIds) {
            if (!(locationId in locations)) {
                needsRefresh = true;
                break;
            }
        }

        if (needsRefresh) {
            this.fetchLocations();
        }
    }

    componentDidMount() {
        this.fetchLocations();
    }

    private asListItem = (organizationWay: OrganizationWay, i: number) => {
        const { onOrganizationWaysChanged, onLocationClick } = this.props;
        if (organizationWay.type === 'internet') {
            return (
                <ListItem
                    onRemove={onOrganizationWaysChanged ?
                        () => this.removeOrganizationWay(organizationWay, i) : undefined}
                    key={`organization-way-${i}`}
                    title={organizationWay.video_stream_url}
                    hint="Link do stream'a"
                    icon={'videocam'}
                />
            );

        }
        if (organizationWay.type === 'location') {
            const { locations } = this.state;
            const location: Location = locations[organizationWay.location_id] as Location;
            return (<ListItem
                onClick={onLocationClick ? () => onLocationClick(location) : undefined}
                onRemove={onOrganizationWaysChanged ?
                    () => this.removeOrganizationWay(organizationWay, i) : undefined}
                key={`organization-way-${i}`}
                title={location ? (location.address.first_line || location.address.second_line) : 'Ładowanie...'}
                hint="Lokalizacja"
                icon={'account_balance'}
            />);
        }
        return null;
    }

    private removeOrganizationWay = (_organizationWay: OrganizationWay, i: number) => {
        const { organizationWays, onOrganizationWaysChanged } = this.props;
        if (!onOrganizationWaysChanged) {
            return;
        }
        const updatedOrganizations = organizationWays.filter((_orgWay, _i) => _i !== i);
        onOrganizationWaysChanged(updatedOrganizations);
    }

    private fetchLocations = async () => {
        const allLocations = await LocationApi.fetchAllLocations();
        const indexed: FetchedLocations = {};
        for (const location of allLocations) {
            indexed[location.id] = location;
        }
        this.setState({ locations: indexed });
    }
}

interface ListItemProps {
    title: string;
    icon: 'videocam' | 'account_balance';
    hint: string;
    onRemove?(): void;
    onClick?(): void;
}
const ListItem = (props: ListItemProps) => {
    const { title, icon, hint, onRemove, onClick } = props;
    return (
        <div onClick={onClick}
            className={`list__item item--dark row content--space-between ${onClick ? 'item--hoverable' : ''}`}>
            <div className="row">
                <div style={{ marginRight: 15 }}>
                    <Avatar suffix={'blue'} ><FontIcon>{icon}</FontIcon></Avatar>
                </div>
                <div>
                    <span className="item-title title--primary">{title}</span>
                    <span className="item-title title--secondary">{hint}</span>
                </div>
            </div>
            {onRemove &&
                <div>
                    <Button onClick={onRemove} icon={true} inkStyle={{ backgroundColor: '#aa7070' }}>
                        <FontIcon style={{ color: '#ff3030' }}>clear</FontIcon>
                    </Button>
                </div>
            }
        </div>
    );
};

export default OrganizationWaysList;
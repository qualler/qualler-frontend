import * as React from 'react';
import { DialogContainer, FontIcon, Button } from 'react-md';
require('./AddOrganizationWayDialog.css');

interface Props {
    open: boolean;
    onClose(): void;
    onInternetOrganization(): void;
    onLocationOrganization(): void;
}

type Phase = 'options' | 'location' | 'internet';
interface State {
    phase: Phase;
}

class AddOrganizationWayDialog extends React.PureComponent<Props, State> {

    state = {
        phase: ('options' as Phase)
    };

    render() {
        const { open, onClose, onInternetOrganization, onLocationOrganization } = this.props;
        return (
            <DialogContainer
                width={800}
                focusOnMount={false}
                id="add-organization-way-dialog"
                visible={open}
                title="Nowy sposób organizacji szkolenia"
                onHide={onClose}>
                <div className="row content--center">
                    <div className="organization-way__options row content--space-between">
                        <OrganizationWayOption
                            onClick={() => {
                                onClose();
                                onLocationOrganization();
                            }}
                            title="Lokalizacja"
                            icon="account_balance"
                        />
                        <OrganizationWayOption
                            onClick={() => {
                                onClose();
                                onInternetOrganization();
                            }}
                            title="Internet"
                            icon="videocam"
                        />
                    </div>
                </div>
            </DialogContainer>
        );
    }
}

interface OrganizationWayOptionProps {
    title: string;
    icon: string;
    onClick?(): void;
}
const OrganizationWayOption = (props: OrganizationWayOptionProps) => {
    const { title, icon, onClick } = props;
    return (
        <div className="organization-way__option column align--center">
            <Button onClick={onClick} primary={true} icon={true} style={{ width: 100, height: 100, marginBottom: 5 }}>
                <FontIcon className="option__button">{icon}</FontIcon>
            </Button>
            <span className="option__title">{title}</span>
        </div>
    );
};

export default AddOrganizationWayDialog;
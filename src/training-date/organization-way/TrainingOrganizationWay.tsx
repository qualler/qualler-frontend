import * as React from 'react';
import { Paper, Toolbar, Checkbox, Button, FontIcon, Snackbar } from 'react-md';
import OrganizationWaysList from './OrganizationWaysList';
import AddOrganizationWayDialog from './AddOrganizationWayDialog';
import InternetOrganizationWayDialog from './InternetOrganizationWayDialog';
import LocationOrganizationWayDialog from './LocationOrganizationWayDialog';
import { OrganizationWay } from '../qualler-types';
import SmallError from '../../errors/SmallError';
require('./TrainingOrganizationWay.css');

interface Props {
    error?: string;
    trainingClosed: boolean;
    organizationWays: OrganizationWay[];
    onClosedChange(trainingClosed: boolean): void;
    onOrganizationChanged(organizationWays: OrganizationWay[]): void;
}

interface State {
    addDialogOpen: boolean;
    internetOrganizationWayDialog: boolean;
    locationOrganizationWayDialog: boolean;
    toasts: Array<{
        text: React.ReactNode,
        action?: React.ReactNode | { onClick?: Function, label: React.ReactNode }
    }>;
}

class TrainingOrganizationWay extends React.PureComponent<Props, State> {

    state = {
        addDialogOpen: false,
        internetOrganizationWayDialog: false,
        locationOrganizationWayDialog: false,
        toasts: []
    };

    render() {
        const { trainingClosed, onClosedChange, organizationWays, onOrganizationChanged, error } = this.props;
        const { addDialogOpen, internetOrganizationWayDialog, locationOrganizationWayDialog } = this.state;
        return (
            <div>
                <Paper>
                    <Toolbar>
                        <div style={{ marginLeft: 50, width: 400, padding: 10 }}>
                            <span className="md-title">Organizacja</span>
                            <SmallError error={error} />
                        </div>
                        <div className="organization-way__training-closed">
                            <Checkbox
                                checked={trainingClosed}
                                onChange={checked => onClosedChange(checked)}
                                name="training-is-open"
                                id="training-is-open"
                                label="Szkolenie zamknięte"
                            />
                        </div>
                    </Toolbar>
                    <div className="organization-way__body">
                        {!trainingClosed &&
                            <AddOrganizationWayButton onClick={() => this.setState({ addDialogOpen: true })} />}
                        {trainingClosed ?
                            <div style={{ padding: 20 }} className="row content--center">
                                <span style={{ fontSize: 14, color: '#cccccc' }}>
                                    Nie dotyczy
                                </span>
                            </div>
                            :
                            organizationWays.length > 0 ?
                                <OrganizationWaysList
                                    onOrganizationWaysChanged={onOrganizationChanged}
                                    organizationWays={organizationWays} />
                                :
                                <div style={{ padding: 20 }} className="row content--center">
                                    <span style={{ fontSize: 14, color: '#d0d0d0' }}>
                                        Nie dodano jeszcze żadnego sposobu organizacji
                                    </span>
                                </div>

                        }

                        <AddOrganizationWayDialog
                            onInternetOrganization={() => this.setState({ internetOrganizationWayDialog: true })}
                            onLocationOrganization={() => this.setState({ locationOrganizationWayDialog: true })}
                            open={addDialogOpen}
                            onClose={() => this.setState({ addDialogOpen: false })} />
                        <InternetOrganizationWayDialog
                            onOrganizationWayCreated={this.organizationWayCreated}
                            open={internetOrganizationWayDialog}
                            onClose={() => this.setState({ internetOrganizationWayDialog: false })}
                        />
                        <LocationOrganizationWayDialog
                            onOrganizationWayCreated={this.organizationWayCreated}
                            open={locationOrganizationWayDialog}
                            onClose={() => this.setState({ locationOrganizationWayDialog: false })}
                        />
                    </div>
                    <Snackbar
                        onDismiss={() => this.setState({ toasts: [] })}
                        id="interactive-snackbar"
                        toasts={this.state.toasts}
                        autohideTimeout={2000}
                    />
                </Paper>
            </div>
        );
    }

    organizationWayCreated = (organizationWay: OrganizationWay) => {
        const { onOrganizationChanged, organizationWays } = this.props;

        if (organizationWay.type === 'internet') {
            if (organizationWays.some(_orgWay => _orgWay.type === 'internet'
                && _orgWay.video_stream_url === organizationWay.video_stream_url)) {
                this.setState({
                    toasts: [{
                        text: 'Taki sposób organizacji już istnieje'
                    }]
                });
                return;
            }
        }
        if (organizationWay.type === 'location') {
            if (organizationWays.some(_orgWay => _orgWay.type === 'location')) {
                this.setState({
                    toasts: [{
                        text: 'Szkolenie może być realizowane tylko w jednej lokalizacji'
                    }]
                });
                return;
            }
        }

        if (onOrganizationChanged) {
            onOrganizationChanged([organizationWay].concat(organizationWays));
        }
    }
}

const AddOrganizationWayButton = (props: { onClick: () => void }) => (
    <Button
        onClick={props.onClick}
        style={{
            width: 40,
            height: 40,
            padding: 10.5,
            position: 'relative',
            marginTop: -20,
            marginLeft: 16,
        }}
        floating={true}
        secondary={true}
        iconEl={<FontIcon>add</FontIcon>}
    />
);

export default TrainingOrganizationWay;
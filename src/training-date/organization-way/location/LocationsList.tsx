import * as React from 'react';
import { Location, Address, formatMoney } from '../../qualler-types';
import { LocationApi } from './LocationApi';
import { List, ListItem, Avatar } from 'react-md';

interface Props {
    onLocationChosen(location: Location): void;
}

interface State {
    allLocations: Location[];
}

class LocationsList extends React.PureComponent<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            allLocations: []
        };
    }

    render() {
        const { allLocations } = this.state;
        const { onLocationChosen } = this.props;
        if (allLocations.length === 0) {
            return (
                <div style={{ padding: 30, color: '#303030' }}>
                    <span>W systemie nie ma obecnie żadnej lokalizacji - w zakładce obok możesz stworzyć nową.</span>
                </div>
            );
        }
        return (
            <List>
                {allLocations.map(location => (
                    <ListItem
                        // tslint:disable-next-line:max-line-length
                        leftAvatar={<Avatar>{(location.address.first_line || location.address.second_line).substring(0, 1).toUpperCase()}</Avatar>}
                        onClick={() => onLocationChosen(location)}
                        primaryText={<span>{location.address.first_line || location.address.second_line}</span>}
                        secondaryText={<span>{this.asAddress(location.address)}</span>}
                        rightAvatar={
                            <span>{formatMoney(location.daily_rental_costs)}</span>
                        }
                        key={`location-${location.id}`} />
                ))}
            </List>
        );
    }

    asAddress(address: Address): string {
        if (!address.first_line) {
            return '';
        }

        return `${address.second_line}, ${address.city}`;
    }

    async componentDidMount() {
        const locations = await LocationApi.fetchAllLocations();
        this.setState({ allLocations: locations });
    }
}

export default LocationsList;
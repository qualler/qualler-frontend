import * as React from 'react';
import { TextField } from 'react-md';
import { Location } from '../../qualler-types';

interface Props {
    actions(saveAction: () => void): React.ReactNode;
    onSuccessfulSubmit(location: Location): void;
}

type FormErrors = { [key: string]: string };

interface State {
    first_line: string;
    second_line: string;
    city: string;
    zipcode: string;
    wholes: string;
    decimals: string;
    errors: FormErrors;
}

class CreateLocationForm extends React.PureComponent<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            first_line: '',
            second_line: '',
            city: '',
            zipcode: '',
            wholes: '',
            decimals: '',
            errors: {
            }
        };
    }

    render() {
        const { actions } = this.props;
        const { first_line, second_line, city, zipcode, wholes, decimals, errors } = this.state;
        return (
            <div>
                <div>
                    <span className="md-title">Adres</span>
                    <div className="column">
                        <TextField
                            value={first_line}
                            onChange={value => this.setState({ first_line: value.toString() })}
                            label="Nazwa budynku (opcjonalnie)"
                            id="address-first-line"
                        />
                        <TextField
                            value={second_line}
                            error={!!(errors.second_line)}
                            errorText={errors.second_line}
                            onChange={value => this.setState({ second_line: value.toString() })}
                            label="Ulica i nr lokalu*"
                            id="address-second-line"
                        />
                    </div>
                    <div className="row content--space-between">
                        <TextField
                            value={city}
                            error={!!(errors.city)}
                            errorText={errors.city}
                            onChange={value => this.setState({ city: value.toString() })}
                            label="Miasto*"
                            id="address-city"
                        />
                        <TextField
                            value={zipcode}
                            error={!!(errors.zipcode)}
                            errorText={errors.zipcode}
                            onChange={value => this.setState({ zipcode: value.toString() })}
                            label="Kod pocztowy*"
                            id="address-zip-code"
                        />
                    </div>
                </div>
                <br />
                <br />
                <div>
                    <span className="md-title">Dzienny koszt wynajęcia</span>
                    <div className="row">
                        <TextField
                            error={!!(errors.wholes)}
                            errorText={errors.wholes}
                            value={wholes}
                            onChange={value => this.setState({ wholes: value.toString() })}
                            style={{ width: 150 }}
                            id="rental-wholes"
                        />
                        <TextField
                            value={decimals}
                            error={!!(errors.decimals)}
                            errorText={errors.decimals}
                            onChange={value => this.setState({ decimals: value.toString() })}
                            style={{ width: 200 }}
                            leftIcon={<span>,</span>}
                            rightIcon={<span>PLN</span>}
                            id="rental-decimals"
                        />
                    </div>
                </div>
                <br />
                {actions(this.save)}
            </div>
        );
    }

    private save = () => {
        const errors = this.validate();
        this.setState({ errors });
        if (Object.keys(errors).length !== 0) {
            return;
        }

        const { first_line, second_line, city, zipcode, wholes, decimals } = this.state;
        const location = {
            id: -1,
            address: {
                first_line,
                second_line,
                city,
                zip_code: zipcode
            },
            daily_rental_costs: {
                wholes: parseInt(wholes, 10),
                decimals: parseInt(decimals, 10),
                currency: 'PLN'
            }
        };

        this.props.onSuccessfulSubmit(location);
    }

    private validate(): FormErrors {
        const { second_line, city, zipcode, wholes, decimals } = this.state;
        const errors = {} as FormErrors;
        if (!second_line || !second_line.trim()) {
            errors.second_line = 'Musisz podać adres';
        }
        if (!city || !city.trim()) {
            errors.city = 'Musisz podać miasto';
        }
        if (!zipcode || !zipcode.trim()) {
            errors.zipcode = 'Musisz podać kod pocztowy';
        }

        if (!wholes) {
            errors.wholes = 'Wartość jest wymagana';
        } else if (!(/^[1-9][0-9]{0,6}$/).test(wholes)) {
            errors.wholes = 'Nieprawidłowa wartość';
        }

        if (!decimals) {
            errors.decimals = 'Wartość jest wymagana';
        } else if (!(/^[0-9]{2}$/).test(decimals)) {
            errors.decimals = 'Nieprawidłowa wartość';
        }
        return errors;
    }

}

export default CreateLocationForm;
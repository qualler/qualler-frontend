import { Location, DetailedTrainingDate } from '../../qualler-types';

const apiOrigin = 'http://localhost:8080';

export class LocationApi {
    static fetchLocation(locationId: number): Promise<Location> {
        return fetch(`${apiOrigin}/api/locations/${locationId}`)
            .then(response => response.json());
    }

    static fetchOrganizedTrainings(locationId: number): Promise<DetailedTrainingDate[]> {
        return fetch(`${apiOrigin}/api/locations/${locationId}/training-dates`)
            .then(response => response.json());
    }

    static async fetchAllLocations(): Promise<Location[]> {
        return fetch(`${apiOrigin}/api/locations`)
            .then(response => response.json());
    }

    static async saveLocation(location: Location) {
        return fetch(`${apiOrigin}/api/locations`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(location)
        });
    }
}
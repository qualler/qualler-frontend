import * as React from 'react';
import { DialogContainer, TabsContainer, Tabs, Tab, Toolbar, Button } from 'react-md';
import LocationsList from './location/LocationsList';
import CreateLocationForm from './location/CreateLocationForm';
import { LocationApi } from './location/LocationApi';
import { Location, OrganizationWay } from '../qualler-types';
require('./LocationOrganizationWayDialog.css');

interface Props {
    open: boolean;
    onClose(): void;
    onOrganizationWayCreated(organizationWay: OrganizationWay): void;
}

interface State {
    activeTab: number;
    showForm: boolean;
}

class LocationOrganizationWayDialog extends React.PureComponent<Props, State> {

    state = {
        activeTab: 0,
        showForm: false
    };

    render() {
        const { open, onClose } = this.props;
        const { activeTab, showForm } = this.state;
        return (

            <DialogContainer
                id="location-organization-way"
                visible={open}
                onHide={onClose}
                width={800}
                paddedContent={false}
                focusOnMount={false}>
                <TabsContainer activeTabIndex={activeTab} onTabChange={this.unmountForm} colored>
                    <Tabs tabId="simple-tab">
                        <Tab className="location-tab" label="Wybierz lokalizację">
                            {activeTab === 0 && <LocationsList
                                onLocationChosen={this.chooseLocation}
                            />}

                        </Tab>
                        <Tab className="location-tab" label="Stwórz nową">
                            <div style={{ padding: 20 }}>
                                {showForm &&
                                    <CreateLocationForm
                                        onSuccessfulSubmit={this.saveLocation}
                                        actions={save => (<div className="row content--end">
                                            <Button onClick={save} flat={true} primary={true}>Zapisz</Button>
                                        </div>)}
                                    />}
                            </div>
                        </Tab>
                    </Tabs>
                </TabsContainer>

            </DialogContainer>
        );
    }

    chooseLocation = (location: Location) => {
        const organizationWay: OrganizationWay = {
            type: 'location',
            location_id: location.id
        };
        this.props.onClose();
        this.props.onOrganizationWayCreated(organizationWay);
    }

    saveLocation = async (location: Location) => {
        await LocationApi.saveLocation(location);
        this.unmountForm(0);
    }

    private unmountForm = (activeTabIndex: number) => {
        if (activeTabIndex === 1) {
            this.setState({ showForm: true, activeTab: 1 });
        } else {
            this.setState({ activeTab: 0 });
            const action = () => {
                this.setState({ showForm: false });
            };
            setTimeout(action, 200);
        }
    }
}

export default LocationOrganizationWayDialog;
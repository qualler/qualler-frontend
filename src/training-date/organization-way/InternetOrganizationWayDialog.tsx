import * as React from 'react';
import { DialogContainer, TextField, Divider, Button } from 'react-md';
import { OrganizationWay } from '../qualler-types';

interface Props {
    open: boolean;
    onClose(): void;
    onOrganizationWayCreated(organizationWay: OrganizationWay): void;
}

// tslint:disable-next-line:max-line-length
const urlRegex = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;

class InternetOrganizationWayDialog extends React.PureComponent<Props, any> {
    render() {
        const { open, onClose, onOrganizationWayCreated } = this.props;
        return (
            <DialogContainer
                id="internet-organization-way"
                visible={open}
                onHide={onClose}
                width={800}
                title="Szkolenie jako stream video"
                focusOnMount={false}>
                <Form
                    onSave={organizationWay => {
                        onClose();
                        onOrganizationWayCreated(organizationWay);
                    }}
                    actions={submitAction => (
                        <div className="row content--space-between">
                            <Button onClick={onClose} flat={true}>Zamknij</Button>
                            <Button onClick={submitAction} flat={true}>Zapisz</Button>
                        </div>
                    )}
                />
            </DialogContainer>
        );
    }
}

interface FormProps {
    actions(submitAction: () => void): React.ReactNode;
    onSave(organizationWay: OrganizationWay): void;
}
class Form extends React.PureComponent<FormProps, any> {

    state = {
        streamUrl: '',
        errors: []
    };

    render() {
        const { streamUrl, errors } = this.state;
        const { actions } = this.props;
        return (
            <div>
                <div>
                    <TextField
                        id="video-stream-link"
                        label="Link do stream'a video"
                        value={streamUrl}
                        error={errors.length > 0}
                        errorText={errors.length > 0 ? errors[0] : undefined}
                        onChange={value => this.setState({ streamUrl: value })} />
                </div>
                <br />
                {actions(this.save)}
            </div>
        );
    }

    private save = () => {
        const { onSave } = this.props;
        const errors = this.validate();
        this.setState({ errors });

        if (errors.length !== 0) {
            return;
        }

        const { streamUrl } = this.state;

        onSave({
            type: 'internet',
            video_stream_url: streamUrl.trim()
        });
    }

    private validate = () => {
        const { streamUrl } = this.state;
        if (!streamUrl || !streamUrl.trim()) {
            return ['Link nie może być pusty'];
        }
        if (!urlRegex.test(streamUrl.trim())) {
            return ['Podany link nie jest prawidłowy'];
        }
        return [];
    }
}

export default InternetOrganizationWayDialog;
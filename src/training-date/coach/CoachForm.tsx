import * as React from 'react';
import { User } from '../qualler-types';
import { TextField } from 'react-md';
import CertificationForm from '../certification/CertificationForm';

interface Props {
    onSuccessfulSubmit(coach: User): void;
    actions(save: () => void): React.ReactNode;
}

type FormErrors = { [key: string]: string };

interface State {
    first_name: string;
    last_name: string;
    email: string;
    wholes: string;
    decimals: string;
    errors: FormErrors;
    certificates: string[];
}

class CoachForm extends React.PureComponent<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            certificates: [],
            first_name: '',
            last_name: '',
            email: '',
            wholes: '',
            decimals: '',
            errors: {}
        };
    }

    render() {
        const { actions } = this.props;
        const { first_name, last_name, email, errors, wholes, decimals, certificates } = this.state;
        return (
            <React.Fragment>
                <form onSubmit={e => { e.preventDefault(); this.save(); }}>
                    <span className="md-title">Dane osobowe</span>
                    <div className="row">
                        <TextField
                            error={!!(errors.first_name)}
                            errorText={errors.first_name}
                            value={first_name}
                            onChange={value => this.setState({ first_name: value.toString() })}
                            label="Imię (imiona)*"
                            id="coach-first-name"
                        />
                        <TextField
                            value={last_name}
                            error={!!(errors.last_name)}
                            errorText={errors.last_name}
                            onChange={value => this.setState({ last_name: value.toString() })}
                            label="Nazwisko*"
                            id="coach-last-name"
                        />
                    </div>
                    <div>
                        <TextField
                            value={email}
                            error={!!(errors.email)}
                            errorText={errors.email}
                            onChange={value => this.setState({ email: value.toString() })}
                            label="Adres e-mail*"
                            id="coach-last-name"
                        />
                    </div>
                    <br />
                    <span className="md-title">Wynagrodzenie</span>
                    <div className="row">
                        <TextField
                            error={!!(errors.wholes)}
                            errorText={errors.wholes}
                            value={wholes}
                            onChange={value => this.setState({ wholes: value.toString() })}
                            style={{ width: 150 }}
                            id="rental-wholes"
                        />
                        <TextField
                            value={decimals}
                            error={!!(errors.decimals)}
                            errorText={errors.decimals}
                            onChange={value => this.setState({ decimals: value.toString() })}
                            style={{ width: 200 }}
                            leftIcon={<span>,</span>}
                            rightIcon={<span>PLN</span>}
                            id="rental-decimals"
                        />
                    </div>
                    <br />
                    <button type="submit" hidden={true}>Wyślij</button>
                </form>
                <span className="md-title">Certyfikaty</span>
                <br />
                <br />
                <div className="row">
                    <CertificationForm
                        certificates={certificates}
                        onCertificatesChange={value => this.setState({ certificates: value })}
                    />
                </div>
                <br />
                {actions(this.save)}
            </React.Fragment>
        );
    }

    private save = () => {
        const errors = this.validate();
        this.setState({ errors });
        if (Object.keys(errors).length !== 0) {
            return;
        }

        const { first_name, last_name, email, wholes, decimals, certificates } = this.state;
        const coach: User = {
            id: -1,
            first_name,
            last_name,
            email,
            profiles: {
                COACH: {
                    type: 'coach',
                    daily_salary: {
                        wholes: parseInt(wholes, 10),
                        decimals: parseInt(decimals, 10),
                        currency: 'PLN'
                    },
                    certificates
                }
            }
        };
        this.props.onSuccessfulSubmit(coach);
    }

    private validate(): FormErrors {
        const { first_name, last_name, email, wholes, decimals } = this.state;
        const errors = {} as FormErrors;
        if (!first_name || !first_name.trim()) {
            errors.first_name = 'Musisz podać imię';
        }
        if (!last_name || !last_name.trim()) {
            errors.last_name = 'Musisz podać nazwisko';
        }
        if (!email || !email.trim()) {
            errors.email = 'Musisz podać adres e-mail';
        // tslint:disable-next-line:max-line-length
        } else if (!(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email))) {
            errors.email = 'Adres e-mail nie jest prawidłowy';
        }

        if (!wholes) {
            errors.wholes = 'Wartość jest wymagana';
        } else if (!(/^[1-9][0-9]{0,6}$/).test(wholes)) {
            errors.wholes = 'Nieprawidłowa wartość';
        }

        if (!decimals) {
            errors.decimals = 'Wartość jest wymagana';
        } else if (!(/^[0-9]{2}$/).test(decimals)) {
            errors.decimals = 'Nieprawidłowa wartość';
        }
        return errors;
    }

}

export default CoachForm;
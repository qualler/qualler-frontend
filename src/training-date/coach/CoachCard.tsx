import * as React from 'react';
import { Paper, Toolbar, Divider, injectInk, List, ListItem, Avatar } from 'react-md';
import { InkProps } from 'react-md/lib/Inks/injectInk';
import { User } from '../qualler-types';
import ChooseCoachDialog from './ChooseCoachDialog';
import SmallError from '../../errors/SmallError';
require('./CoachCard.css');

interface Props {
    coach: User | null;
    error?: string;
    onCoachChosen(coach: User): void;
}

interface State {
    dialogOpen: boolean;

}

class CoachCard extends React.PureComponent<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            dialogOpen: false
        };
    }

    render() {
        const { dialogOpen } = this.state;
        const { onCoachChosen, coach, error } = this.props;
        return (
            <Paper>
                <Toolbar>
                    <div style={{ padding: 15 }}>
                        <span className="md-title">Trener</span>
                        <SmallError error={error} />
                    </div>
                </Toolbar>
                <Divider />
                <div>
                    {!coach && <div style={{ padding: 20 }}>
                        <ChooseCoachButton onClick={() => this.setState({ dialogOpen: true })} />
                    </div>}
                    {coach &&
                        <div>
                            <List>
                                <ListItem
                                    activeBoxStyle={{ padding: 15 }}
                                    // tslint:disable-next-line:max-line-length
                                    leftAvatar={<Avatar>{(coach.first_name.substring(0, 1) + coach.last_name.substring(0, 1).toUpperCase())}</Avatar>}
                                    onClick={() => this.setState({ dialogOpen: true })}
                                    // tslint:disable-next-line:max-line-length
                                    primaryText={<span className="md-subheading-1">{coach.first_name} {coach.last_name}</span>}
                                    key={`coach-${coach.id}`} />
                            </List>
                        </div>
                    }
                    <ChooseCoachDialog
                        onCoachChosen={onCoachChosen}
                        open={dialogOpen}
                        onClose={() => this.setState({ dialogOpen: false })}
                    />
                </div>
            </Paper>
        );
    }
}

interface ButtonProps {
    onClick?(): void;
}
const ChooseCoachButton = injectInk<ButtonProps>((props: InkProps & ButtonProps) => (
    <div onClick={props.onClick} className={`column align--center content--center choose-coach-button`}>
        <span className="choose-coach-button__text">Wybierz trenera</span>
        {props.ink}
    </div>
));

export default CoachCard;
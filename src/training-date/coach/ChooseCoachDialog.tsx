import * as React from 'react';
import { DialogContainer, TabsContainer, Tabs, Tab, Button } from 'react-md';
import CoachesList from './CoachesList';
import CoachForm from './CoachForm';
import { User } from '../qualler-types';
import { CoachApi } from './CoachApi';

interface Props {
    open: boolean;
    onClose(): void;
    onCoachChosen(coach: User): void;
}

class ChooseCoachDialog extends React.PureComponent<Props, any> {

    state = {
        activeTab: 0,
        showForm: true
    };

    render() {
        const { open, onClose, onCoachChosen } = this.props;
        const { activeTab, showForm } = this.state;
        return (
            <DialogContainer
                id="choose-coach"
                visible={open}
                onHide={onClose}
                width={800}
                paddedContent={false}
                focusOnMount={false}>
                <TabsContainer
                    activeTabIndex={activeTab}
                    onTabChange={this.unmountForm}
                    colored>
                    <Tabs tabId="simple-tab">
                        <Tab className="location-tab" label="Wybierz trenera">
                            {activeTab === 0 &&
                                <CoachesList
                                    onCoachChosen={coach => {
                                        onClose();
                                        onCoachChosen(coach);
                                    }}
                                />}

                        </Tab>
                        <Tab className="location-tab" label="Wprowadź nowego trenera">
                            <div style={{ padding: 20 }}>
                                {showForm &&
                                    <CoachForm
                                        onSuccessfulSubmit={this.saveCoach}
                                        actions={save => (<div className="row content--end">
                                            <Button onClick={save} flat={true} primary={true}>Zapisz</Button>
                                        </div>)}
                                    />}
                            </div>
                        </Tab>
                    </Tabs>
                </TabsContainer>

            </DialogContainer>
        );
    }

    private saveCoach = async (coach: User) => {
        await CoachApi.createCoach(coach);
        this.unmountForm(0);
    }

    private unmountForm = (activeTabIndex: number) => {
        if (activeTabIndex === 1) {
            this.setState({ showForm: true, activeTab: 1 });
        } else {
            this.setState({ activeTab: 0 });
            const action = () => {
                this.setState({ showForm: false });
            };
            setTimeout(action, 200);
        }
    }
}

export default ChooseCoachDialog;
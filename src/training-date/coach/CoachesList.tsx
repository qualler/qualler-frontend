import * as React from 'react';
import { CoachApi } from './CoachApi';
import { User } from '../qualler-types';
import { List, ListItem, Avatar, Chip } from 'react-md';
require('./CoachesList.css');

interface Props {
    onCoachChosen(coach: User): void;
}
interface State {
    coaches: User[];
}
class CoachesList extends React.PureComponent<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            coaches: []
        };
    }

    render() {
        const { coaches } = this.state;

        if (coaches.length === 0) {
            return (
                <div style={{ padding: 20 }}>
                    <span className="md-body-1">
                        W systemie nie ma ani jednego trenera - możesz go wprowadzić w karcie obok
                    </span>
                </div>
            );
        }

        const { onCoachChosen } = this.props;
        return (
            <List>
                {coaches.map(coach => (
                    <ListItem
                        className="coaches-list__item"
                        // tslint:disable-next-line:max-line-length
                        leftAvatar={<Avatar>
                            {(coach.first_name.substring(0, 1) + coach.last_name.substring(0, 1).toUpperCase())}
                        </Avatar>}
                        onClick={() => onCoachChosen(coach)}
                        primaryText={
                            <div className="row content--space-between">
                                <span className="md-subheading-1">
                                    {coach.first_name} {coach.last_name} ({coach.email})
                                </span>
                                <span>
                                    {coach.profiles.COACH && (coach.profiles.COACH.daily_salary.wholes)},
                                        {coach.profiles.COACH && (coach.profiles.COACH.daily_salary.decimals)}&nbsp;
                        PLN
                                </span>
                            </div>
                        }
                        secondaryText={
                            <div>
                                {coach.profiles.COACH && coach.profiles.COACH.certificates
                                    .map(certificate =>
                                        <Chip
                                            removable={false}
                                            style={{ backgroundColor: '#00aaff', marginRight: 5 }}
                                            key={certificate}
                                            label={certificate} />)}
                            </div>
                        }
                        key={`coach-${coach.id}`} />
                ))}
            </List>
        );
    }

    async componentDidMount() {
        const coaches = await CoachApi.fetchCoaches();
        this.setState({ coaches });
    }
}

export default CoachesList;
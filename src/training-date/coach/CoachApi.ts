import { User, DetailedTrainingDate } from '../qualler-types';

const apiOrigin = 'http://localhost:8080';

export class CoachApi {
    static fetchConductedTrainings(coachId: number): Promise<DetailedTrainingDate[]> {
        return fetch(`${apiOrigin}/api/users/${coachId}/coach/training-dates`)
            .then(response => response.json());
    }
    static fetchCoach(coachId: number): Promise<User> {
        return fetch(`${apiOrigin}/api/users/${coachId}/coach`)
            .then(response => response.json());
    }
    static async createCoach(coach: User) {
        return fetch(`${apiOrigin}/api/users`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(coach)
        });
    }
    static async fetchCoaches(): Promise<User[]> {
        return fetch(`${apiOrigin}/api/users/COACH`)
            .then(response => response.json());
    }
}
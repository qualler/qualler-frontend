import * as React from 'react';
import { Paper, Toolbar, Divider, Switch, Chip, TextField } from 'react-md';
import { CertificationPolicy } from '../qualler-types';
import CertificationForm from './CertificationForm';
import SmallError from '../../errors/SmallError';

interface Props {
    error?: string;
    certificationPolicy: CertificationPolicy;
    onCertificationPolicyChange(certificationPolicy: CertificationPolicy): void;
}

class CertificationCard extends React.PureComponent<Props, any> {
    render() {
        const { certificationPolicy, error } = this.props;
        return (
            <Paper>
                <Toolbar>
                    <div style={{ padding: 15 }}>
                        <span className="md-title">Certyfikacja</span>
                        <SmallError error={error} />
                    </div>
                </Toolbar>
                <Divider />
                <div style={{ padding: 20 }}>
                    <Switch
                        onChange={this.toggleCertification}
                        id="certification-enabled"
                        name="certification-enabled"
                        label="Szkolenie certyfikowane"
                        checked={certificationPolicy.type === 'certification-required'} />
                </div>
                {certificationPolicy.type === 'certification-required' &&
                    <React.Fragment>
                        <Divider />
                        <div style={{ padding: 20 }}>
                            <span>Certyfikaty</span>
                            <br />
                            <br />
                            <CertificationForm
                                certificates={certificationPolicy.certificates}
                                onCertificatesChange={this.publishCertificates}
                            />
                        </div>
                    </React.Fragment>
                }
            </Paper>
        );
    }

    private publishCertificates = (certificates: string[]) => {
        const { onCertificationPolicyChange } = this.props;
        onCertificationPolicyChange({
            type: 'certification-required',
            certificates
        });
    }

    private toggleCertification = (checked: boolean) => {
        const { onCertificationPolicyChange } = this.props;
        if (!checked) {
            onCertificationPolicyChange({ type: 'no-certification' });
        } else {
            onCertificationPolicyChange({ type: 'certification-required', certificates: [] });
        }
    }
}

export default CertificationCard;
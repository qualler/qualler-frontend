import * as React from 'react';
import { Chip, TextField } from 'react-md';

interface Props {
}

interface FormProps {
    certificates: string[];
    onCertificatesChange(certificates: string[]): void;
}
class CertificationForm extends React.PureComponent<FormProps, any> {

    state = {
        _certificate: ''
    };

    render() {
        const { certificates } = this.props;
        const { _certificate } = this.state;
        return (
            <form onSubmit={e => {
                e.preventDefault();
                this.onSubmit();
            }}>
                <div>
                    {certificates.length > 0 ?
                        certificates.map((certificate, i) => (
                            <Chip
                                style={{ marginRight: 5 }}
                                label={certificate}
                                key={`certificate-${i}`}
                                onClick={() => this.remove(certificate)}
                                removable={true} />
                        ))
                        :
                        <div>
                            <span style={{color: '#505050'}}>Brak certyfikatów</span>
                        </div>
                    }

                    <TextField
                        style={{ width: 200 }}
                        value={_certificate}
                        onChange={value => this.setState({ _certificate: value })}
                    />
                </div>
            </form>
        );
    }

    private remove = (certificate: string) => {
        const { certificates, onCertificatesChange } = this.props;
        onCertificatesChange(certificates.filter(_certificate => _certificate !== certificate));
    }

    private onSubmit = () => {
        const { _certificate } = this.state;
        const { certificates, onCertificatesChange } = this.props;

        if (!_certificate || !_certificate.trim()) {
            return;
        }
        if (certificates.indexOf(_certificate.trim()) !== -1) {
            return;
        }
        this.setState({ _certificate: '' });
        onCertificatesChange([_certificate.trim()].concat(certificates));
    }
}

export default CertificationForm;
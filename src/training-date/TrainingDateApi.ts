import { TrainingDate, DetailedTrainingDate } from './qualler-types';

const apiOrigin = 'http://localhost:8080';

export class TrainingDateApi {
    static async fetchTrainingDate(trainingDateId: number): Promise<DetailedTrainingDate> {
        return fetch(`${apiOrigin}/api/training-dates/${trainingDateId}`)
            .then(response => response.json());
    }
    static async createTrainingDate(trainingDate: TrainingDate) {
        return fetch(`${apiOrigin}/api/training-dates`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(trainingDate)
        });
    }

    static async fetchAllTrainingDates(): Promise<DetailedTrainingDate[]> {
        return fetch(`${apiOrigin}/api/training-dates`)
            .then(response => response.json());
    }

    static fetchTrainingDates(trainingId: number): Promise<DetailedTrainingDate[]> {
        return fetch(`${apiOrigin}/api/trainings/${trainingId}/training-dates`)
            .then(response => response.json());
    }
}
import * as React from 'react';
import TrainingsList from './TrainingsList';
import { TrainingDateApi } from '../training-date/TrainingDateApi';
import { DetailedTrainingDate } from '../training-date/qualler-types';
import { Card, Toolbar } from 'react-md';
require('./TrainingsPage.css');

interface Props {
}

interface State {
    trainings: DetailedTrainingDate[];
}

class TrainingsPage extends React.PureComponent<Props, State> {

    state = {
        trainings: []
    };

    render() {
        const { trainings } = this.state;
        return (
            <div className="trainings-page">
                
                <div style={{ marginTop: 80, width: '90%' }}>
                    <Card>
                        <Toolbar colored={true} title="Terminy szkoleń" />
                        <TrainingsList trainings={trainings} />
                    </Card>
                </div>
            </div>
        );
    }

    async componentDidMount() {
        const trainingDates = await TrainingDateApi.fetchAllTrainingDates();
        this.setState({ trainings: trainingDates });
    }
}

export default TrainingsPage;
import * as React from 'react';
import { Card, List, ListItem, Divider, FontIcon, Avatar } from 'react-md';
import { DetailedTrainingDate } from '../training-date/qualler-types';
import { TrainingDateApi } from '../training-date/TrainingDateApi';
import * as moment from 'moment';
import { RouteComponentProps, withRouter } from 'react-router';

require('./TrainingsList.css');

interface Props {
    trainings: DetailedTrainingDate[];
}

interface State {
    trainings: DetailedTrainingDate[];
}

class TrainingsList extends React.PureComponent<Props & RouteComponentProps<any>, State> {

    render() {
        const { trainings, history } = this.props;
        return (
            <List className="training-list__trainings">
                {trainings.map(training => (
                    <TrainDateListItem
                        onClick={() => history.push(`/trainings/${training.id}`)}
                        training={training}
                        key={`training-date-${training.id}`} />
                ))}
            </List>
        );
    }
}

const TrainDateListItem = (props: { training: DetailedTrainingDate, onClick(): void }) => {
    const { training, onClick } = props;
    return (
        <ListItem
            style={{ borderBottom: '#ccc solid 1px', width: '100%' }}
            onClick={onClick}
            primaryTextStyle={{ marginBottom: 10 }}
            primaryText={<span className="md-title" style={{ marginBottom: 10 }}>
                {training.training.name}
            </span>}
            secondaryText={
                <div className="column">
                    {training.open ? <TrainingOpen /> : <TrainingClosed />}
                    <div className="row align--center">
                        <FontIcon style={{ marginRight: 5 }}>account_circle</FontIcon>
                        <span>{training.coach.first_name} {training.coach.last_name}</span>
                    </div>
                </div>
            }
            rightAvatar={
                <span>
                    {moment(training.start_date).format('DD-MM-YYYY')}
                    &nbsp;-&nbsp;
                    {moment(training.end_date).format('DD-MM-YYYY')}
                </span>
            }
        />
    );
};

const TrainingClosed = () => (
    <div className="row align--center">
        <FontIcon style={{ marginRight: 5 }}>lock</FontIcon>
        <span>Szkolenie zamknięte</span>
    </div>
);

const TrainingOpen = () => (
    <div className="row align--center">
        <FontIcon style={{ marginRight: 5 }}>lock_open</FontIcon>
        <span>Szkolenie otwarte</span>
    </div>
);

export default withRouter(TrainingsList);
import * as React from 'react';
import { DetailedTrainingDate, Location } from '../../training-date/qualler-types';
import { Card, Toolbar } from 'react-md';
import OrganizationWaysList from '../../training-date/organization-way/OrganizationWaysList';

interface Props {
    trainingDate: DetailedTrainingDate;
    onLocationClick(location: Location): void;
}

class TrainingDateOrganizationWays extends React.PureComponent<Props, any> {
    render() {
        const { trainingDate, onLocationClick } = this.props;
        return (
            <Card>
                <Toolbar title="Sposób organizacji" colored />
                <OrganizationWaysList
                    onLocationClick={onLocationClick}
                    organizationWays={trainingDate.organization_ways}
                />
            </Card>
        );
    }
}

export default TrainingDateOrganizationWays;
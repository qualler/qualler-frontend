import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { TrainingDateApi } from '../../training-date/TrainingDateApi';
import { DetailedTrainingDate } from '../../training-date/qualler-types';
import { Card, Divider, Avatar, FontIcon, Toolbar } from 'react-md';
import * as moment from 'moment';
import TrainingDateOrganizationWays from './TrainingDateOrganizationWays';
import TrainingsList from '../TrainingsList';
require('./TrainingDetailsPage.css');

interface Props {
}

interface State {
    trainingDate: DetailedTrainingDate | null;
    otherDates: DetailedTrainingDate[];
}

interface UrlProps {
    trainingDateId: string;
}

class TrainingDetailsPage extends React.PureComponent<Props & RouteComponentProps<UrlProps>, State> {

    constructor(props: any) {
        super(props);
        this.state = {
            trainingDate: null,
            otherDates: []
        };
    }

    render() {
        const { trainingDate, otherDates } = this.state;
        const { history } = this.props;
        if (!trainingDate) {
            return null;
        }

        return (
            <div className="training-date-details__page row content--center">
                <div className="app-content-column">
                    <TrainingDateHeader
                        onCoachClick={() => history.push(`/coaches/${trainingDate.coach_id}`)}
                        trainingDate={trainingDate} />
                    <br />
                    {trainingDate.open && <TrainingDateOrganizationWays
                        onLocationClick={(location) => history.push(`/locations/${location.id}`)}
                        trainingDate={trainingDate} />}
                    <br />
                    <OtherTrainingDates
                        trainingDates={otherDates
                            .filter(_trainingDate => _trainingDate.id !== trainingDate.id)} />
                </div>
            </div>
        );
    }

    componentDidUpdate(prevProps: Props & RouteComponentProps<UrlProps>, prevState: State) {
        if (this.props.match.params.trainingDateId !== prevProps.match.params.trainingDateId) {
            this.fetchData();
        }
    }

    componentDidMount() {
        this.fetchData();
    }

    private async fetchData() {
        const { match } = this.props;
        const trainingDateId = parseInt(match.params.trainingDateId, 10);
        const trainingDate = await TrainingDateApi.fetchTrainingDate(trainingDateId);
        this.setState({ trainingDate });
        const trainingDates = await TrainingDateApi.fetchTrainingDates(trainingDate.training_id);
        this.setState({ otherDates: trainingDates });
    }
}

const TrainingDateHeader = (props: { trainingDate: DetailedTrainingDate, onCoachClick(): void }) => (
    <Card>
        <div style={{ padding: 25 }}>
            <span className="md-display-1">{props.trainingDate.training.name}</span>
        </div>
        <Divider />
        <div className="row content--center align--center" style={{ padding: 15 }}>
            <InfoSegment icon="attach_money">
                {props.trainingDate.price.wholes},
                {props.trainingDate.price.decimals < 10 ? '0' + props.trainingDate.price.decimals :
                    props.trainingDate.price.decimals}
                &nbsp;
                {props.trainingDate.price.currency}
            </InfoSegment>
            <InfoSegment icon="event">
                {moment(props.trainingDate.start_date).format('DD-MM-YYYY')}
                &nbsp;-&nbsp;
                    {moment(props.trainingDate.end_date).format('DD-MM-YYYY')}
            </InfoSegment>
            <InfoSegment onClick={props.onCoachClick} icon="account_circle">
                {props.trainingDate.coach.first_name} {props.trainingDate.coach.last_name}
            </InfoSegment>
            <InfoSegment icon={props.trainingDate.open ? 'lock_open' : 'lock'}>
                {props.trainingDate.open ? 'Szkolenie otwarte' : 'Szkolenie zamnięte'}
            </InfoSegment>
            <InfoSegment icon="card_membership">
                {props.trainingDate.certification_policy.type === 'no-certification' ?
                    'Szkolenie niecertyfikowane'
                    :
                    `Certyfikacja: ${props.trainingDate.certification_policy.certificates.join(', ')}`
                }
            </InfoSegment>
        </div>
    </Card>
);

const InfoSegment = (props: { icon: string, children: any, onClick?(): void }) => (
    <div onClick={props.onClick}
        className={`info-segment column align--center ${props.onClick ? 'info-segment--hoverable' : ''}`}>
        <Avatar className="info-segment__avatar" suffix="blue">
            <FontIcon>{props.icon}</FontIcon>
        </Avatar>
        <span className="info-segment__text">{props.children}</span>
    </div>
);

const OtherTrainingDates = (props: { trainingDates: DetailedTrainingDate[] }) => (
    <Card className="column">
        <Toolbar colored={true} title="Pozostałe terminy tego szkolenia" />
        {props.trainingDates.length > 0 ?

            <TrainingsList trainings={props.trainingDates} />
            :
            <div style={{ padding: 20 }}>
                <span className="md-body-1">W systemie nie ma innych terminów tego szkolenia</span>
            </div>
        }
    </Card>
);

export default TrainingDetailsPage;
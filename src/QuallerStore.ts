import { createStore, Action, Reducer } from 'redux';

interface QuallerState {
}

const initialState: QuallerState = {
};

type Reducers = {
    [key: string]: (state: QuallerState, action: any) => QuallerState
};

const quallerReducers: Reducers = {
    'TRAINING_CREATED'(state: QuallerState, action: any) {
        return state;
    }
};
const flatReducer = (reducers: Reducers): Reducer<QuallerState, any> => {
    return (state: QuallerState, action: Action<string>) => {
        const reducer = reducers[action.type];
        if (reducer) {
            return reducer(state, action);
        }
        return state;
    };
};

export const store = createStore<QuallerState, any, any, any>(flatReducer(quallerReducers), initialState);
